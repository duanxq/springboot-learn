```mermaid
graph LR
start(start)-->pre
invoke1[invoke]-.->pre
pre --发生异常--> conndition{重试任务?}
subgraph retryAspect
conndition --新建任务--> taskCache
taskCache --通知-->queue((队列 ))
taskCache --保存任务-->db((数据库))
A[重试任务执行器] -.注册.-> queue
A --> invoke
A --query--> taskCache
invoke --更新任务--> taskCache
end
conndition --存在--> next
next-->结束(end)
```
### 流程图
![graph](./img/retry.png)
