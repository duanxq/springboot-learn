CREATE TABLE `retry`
(
    `id`         int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `startTime`  datetime      NOT NULL COMMENT '任务开始时间',
    `retryTime`  datetime      NOT NULL COMMENT '下次重试时间',
    `className`  varchar(255)  NOT NULL,
    `methodName` varchar(255)  NOT NULL,
    `argsType`   varchar(2000) NOT NULL,
    `args`       varchar(2000) NOT NULL,
    `retryCount` int(11)       NOT NULL,
    `status`     int(11)       NOT NULL DEFAULT '1' COMMENT '状态，1：待重试，2：重试中，3：失败，4：成功',
    `message`    varchar(255)           DEFAULT '' COMMENT '失败原因',
    `uuid`       varchar(100)  NOT NULL COMMENT 'uuid',
    `sign`       varchar(50)   NOT NULL COMMENT '方法签名',
    PRIMARY KEY (`id`),
    KEY `idx_uuid` (`uuid`),
    KEY `idx_sign` (`sign`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;