package com.springboot.learn.retry.repository.db;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

/**
 * @author duanxq
 * Created by duanxq on 2019-09-20.
 */
@Repository
public interface RetryMapper extends Mapper<Retry>, MySqlMapper<Retry> {

    @Select("select * from retry where status in (1, 2)")
    List<Retry> selectTask();

    @Select("select count(*) from retry where status in (1, 2) and sign = #{sign}")
    boolean taskExist(String sign);

}
