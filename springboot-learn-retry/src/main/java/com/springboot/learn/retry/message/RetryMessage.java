package com.springboot.learn.retry.message;

import java.util.Observable;

/**
 * @author BG343674
 * created by BG343674 on 2019/11/24
 */

public abstract class RetryMessage extends Observable {

    /**
     * 在指定时间后消费
     * @param obj
     * @param time
     */
    public abstract void consumeAt(Object obj, Long time);



}
