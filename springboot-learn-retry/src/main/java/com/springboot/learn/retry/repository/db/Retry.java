package com.springboot.learn.retry.repository.db;

import lombok.Data;

import javax.persistence.Id;
import java.util.Date;

@Data
public class Retry {
    /**
     * 主键
     */
    @Id
    private Integer id;

    /**
     * uuid
     */
    private String uuid;

    /**
     * 任务开始时间
     */
    private Date startTime;

    /**
     * 下次重试时间
     */
    private Date retryTime;

    private String className;

    private String methodName;

    private String argsType;

    private String args;

    private String sign;

    private Integer retryCount;

    private String message;

    private Integer status;

}