package com.springboot.learn.retry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.spring.annotation.MapperScan;

import java.util.Random;

@RestController
@MapperScan(basePackages = "com.springboot.learn.retry.repository.db")
@SpringBootApplication
public class RetryApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetryApplication.class, args);
    }

    @Retry
    @RequestMapping("test1")
    public Object test(String i) throws Exception {
        boolean b = new Random().nextBoolean();
        if (b) {
            throw new Exception("exception info");
        }
        return "success";
    }



}
