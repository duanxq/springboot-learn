package com.springboot.learn.retry;

import java.lang.annotation.*;

/**
 * @author BG343674
 * created by BG343674 on 2019/9/19
 */
@Documented
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Retry {

    /**
     * 功能描述
     * @return
     */
    String des() default "";

}
