package com.springboot.learn.retry;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class TestController {

    @Retry
    @RequestMapping("test")
    public Object test() throws Exception {
        boolean b = new Random().nextBoolean();
        if (b) {
            throw new Exception("exception info");
        }
        return "success";
    }


}
