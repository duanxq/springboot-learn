package com.springboot.learn.retry.repository;

import com.springboot.learn.retry.model.MyRetryContext;

/**
 * @author BG343674
 * created by BG343674 on 2019/11/23
 */
public interface RetryTaskRepository {

    /**
     * 重试任务是否存在
     * @param sign
     * @return
     */
    boolean taskExist(String sign);

    /**
     * 获取重试任务
     * @param key
     * @return
     */
    MyRetryContext get(Object key);

    /**
     * 保存重试任务
     * @param value
     */
    void save(MyRetryContext value);

    /**
     * 修改重试任务
     * @param retryContext
     */
    void updateStatus(MyRetryContext retryContext);

}
